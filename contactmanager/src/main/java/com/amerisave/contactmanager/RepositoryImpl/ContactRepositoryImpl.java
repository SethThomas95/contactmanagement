package com.amerisave.contactmanager.RepositoryImpl;

import com.amerisave.contactmanager.Beans.Contact;
import com.amerisave.contactmanager.Repository.ContactRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ContactRepositoryImpl implements ContactRepository {
    private static List<Contact> contactList = new ArrayList<Contact>();


    @Override
    public List<Contact> getContacts() {
        return contactList;
    }

    @Override
    public Integer addContact(Contact contact) {
        if(contactList.contains(contact)) return 0;
        else {
            contactList.add(contact);
            return 1;
        }
    }

    @Override
    public Integer editContact(Contact current, Contact update) {
        if(contactList.contains(current)){
            if(!contactList.contains(update)) {
                contactList.remove(current);
                contactList.add(update);
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return -1;
        }
    }

    @Override
    public Integer removeContact(Contact contact) {
        if(contactList.contains(contact)){
            contactList.remove(contact);
            return 1;
        }
        else return 0;
    }
}
