package com.amerisave.contactmanager.Repository;

import com.amerisave.contactmanager.Beans.Contact;

import java.util.List;

public interface ContactRepository {
    List<Contact> getContacts();
    Integer addContact(Contact contact);
    Integer editContact(Contact current, Contact update);
    Integer removeContact(Contact contact);
}
