package com.amerisave.contactmanager.Controllers;

import com.amerisave.contactmanager.Beans.Contact;
import com.amerisave.contactmanager.Beans.UpdateContact;
import com.amerisave.contactmanager.Service.ContactService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="Contacts resource", produces="application/json")
@RequestMapping("/contacts")
public class ContactController {
    @Autowired
    private ContactService contactService;

    @ApiOperation(value = "Retrieve Contacts")
    @GetMapping("/getContacts")
    @ResponseBody
    public ResponseEntity getContacts() { return contactService.getContacts();}

    @ApiOperation(value = "Add Contact")
    @PostMapping("/addContact")
    @ResponseBody
    public ResponseEntity addContact(@RequestBody Contact contact) {
        return contactService.addContact(contact);
    }

    @ApiOperation(value = "Edit Contact")
    @PostMapping("/editContact")
    @ResponseBody
    public ResponseEntity editContact(@RequestBody UpdateContact updateContact) {
        return contactService.editContact(updateContact.getCurrent(),updateContact.getUpdate());
    }

    @ApiOperation(value = "Remove Contact")
    @PostMapping("/removeContact")
    @ResponseBody
    public ResponseEntity removeContact(@RequestBody Contact contact) {
        return contactService.removeContact(contact);
    }
}
