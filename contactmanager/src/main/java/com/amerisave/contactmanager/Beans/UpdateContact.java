package com.amerisave.contactmanager.Beans;

import lombok.Getter;

@Getter
public class UpdateContact {
    private Contact current;
    private Contact update;

    public Contact getCurrent(){
        return current;
    }

    public Contact getUpdate(){
        return update;
    }
}
