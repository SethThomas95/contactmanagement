package com.amerisave.contactmanager.Beans;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Contact {
    private String firstName;
    private String lastName;
    private String email;


    @Override
    public String toString(){
        return "First name: " + firstName + " " +
                "Last Name: " + lastName + " " +
                "Email: " + email + " ";
    }
    @Override
    public boolean equals(Object o){
        if(o == this) return true;
        if(!(o instanceof Contact)) return false;
        Contact contact = (Contact) o;
        return contact.firstName.equals(firstName) &&
                contact.lastName.equals(lastName) &&
                contact.email.equals(email);
    }

    @Override
    public int hashCode(){
        int result = 17;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
