package com.amerisave.contactmanager.Service;

import com.amerisave.contactmanager.Beans.Contact;
import com.amerisave.contactmanager.Repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ContactService {
    @Autowired
    private ContactRepository contactRepository;

    public ResponseEntity getContacts(){
        try {
            System.out.println("Retrieving contacts...");
            List<Contact> contacts = contactRepository.getContacts();
            Map<String,Object> responseMap = new HashMap();
            responseMap.put("retrieveContactsResponse",contacts);
            return new ResponseEntity(responseMap,HttpStatus.OK);
        }
        catch(Exception e){
            System.out.println("Failed to retrieve contacts: " + e.getMessage());
            return new ResponseEntity("Failed to retrieve contacts", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity addContact(Contact contact){
        try {
            System.out.println("Adding contact: " + contact.toString());
            int success = contactRepository.addContact(contact);
            Map<String,Object> responseMap = new HashMap();
            if(success==1){
                responseMap.put("addContactResponse","Successfully added contact: " + contact.toString());
                return new ResponseEntity(responseMap,HttpStatus.OK);
            }
            else{
                responseMap.put("addContactResponse","Contact already exists: " + contact.toString());
                return new ResponseEntity(responseMap,HttpStatus.BAD_REQUEST);
            }
        }
        catch(Exception e){
            System.out.println("Failed to retrieve contact: " + e.getMessage());
            return new ResponseEntity("Failed to add contact", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity editContact(Contact current,Contact update){
        try {
            System.out.println("Editing contact: " + current.toString() + " To: " + update.toString());
            int success = contactRepository.editContact(current,update);
            Map<String,Object> responseMap = new HashMap();
            if(success==1){
                responseMap.put("editContactResponse","Successfully updated contact: " + current.toString() + " To: " + update.toString());
                return new ResponseEntity(responseMap,HttpStatus.OK);
            }
            else if(success==0){
                responseMap.put("editContactResponse","Contact already exists: " + update.toString());
                return new ResponseEntity(responseMap,HttpStatus.BAD_REQUEST);
            }
            else{
                responseMap.put("editContactResponse","Contact does not exist: " + current.toString());
                return new ResponseEntity(responseMap,HttpStatus.BAD_REQUEST);
            }
        }
        catch(Exception e){
            System.out.println("Failed to edit contact: " + e.getMessage());
            return new ResponseEntity("Failed to add contact", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity removeContact(Contact contact){
        try {
            System.out.println("Removing contact: " + contact.toString());
            int success = contactRepository.removeContact(contact);
            Map<String,Object> responseMap = new HashMap();
            if(success==1){
                responseMap.put("removeContactResponse","Successfully removed contact: " + contact.toString());
                return new ResponseEntity(responseMap,HttpStatus.OK);
            }
            else{
                responseMap.put("removeContactResponse","Contact does not exist: " + contact.toString());
                return new ResponseEntity(responseMap,HttpStatus.BAD_REQUEST);
            }
        }
        catch(Exception e){
            System.out.println("Failed to remove contact: " + e.getMessage());
            return new ResponseEntity("Failed to add contact", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
